<?php

/**
 * @file
 * Coupon rules rules integration file.
 */

/**
 * Implements hook_rules_conditions_info()
 */
function commerce_coupon_couponrules_rules_condition_info() {
  $conditions = array();

  // Condition to check if a particular coupon exists in an order.
  $conditions['commerce_coupon_couponrules_condition_coupon_exists'] = array(
    'label' => t('Order contains a particular coupon code'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose coupons should be checked.'),
      ),
      'coupon_id' => array(
        'type' => 'text',
        'label' => t('Coupon code'),
        'description' => t('The code of the coupon to look for on the order.'),
      ),
    ),
    'group' => t('Commerce Coupon'),
    'callbacks' => array(
      'execute' => 'commerce_coupon_couponrules_condition_coupon_exists',
    ),
  );

  // Condition to check if a list of coupon codes exists in an order.
  $conditions['commerce_coupon_couponrules_condition_coupons_exists'] = array(
    'label' => t('Order contains any coupon code from a list'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose coupons should be checked.'),
      ),
      'coupon_id' => array(
        'type' => 'list<text>',
        'label' => t('Coupon code'),
        'description' => t('The list of coupon codes to look for on the order.'),
      ),
    ),
    'group' => t('Commerce Coupon'),
    'callbacks' => array(
      'execute' => 'commerce_coupon_couponrules_condition_coupons_exists',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_coupon_couponrules_rules_action_info() {

  module_load_include('module', 'commerce_coupon_couponrules');

  $actions = array();

  $actions['commerce_coupon_couponrules_action_validate'] = array(
    'label' => t('Validate individual coupon'),
    'parameter' => commerce_coupon_couponrules_component_variables(),
    'group' => t('Commerce Coupon'),
    'base' => 'commerce_coupon_couponrules_action_validate',
    'callbacks' => array(
      'execute' => 'commerce_coupon_couponrules_action_validate',
    ),
  );
  $actions['commerce_coupon_couponrules_action_is_valid_coupon'] = array(
    'label' => t('Set coupon as valid'),
    'parameter' => array(
      'coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
    ),
    'group' => t('Commerce Coupon'),
    'base' => 'commerce_coupon_couponrules_action_is_valid_coupon',
    'callbacks' => array(
      'execute' => 'commerce_coupon_couponrules_action_is_valid_coupon',
    ),
  );
  $actions['commerce_coupon_couponrules_action_is_invalid_coupon'] = array(
    'label' => t('Set coupon as invalid'),
    'parameter' => array(
      'coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
    ),
    'group' => t('Commerce Coupon'),
    'base' => 'commerce_coupon_couponrules_action_is_invalid_coupon',
    'callbacks' => array(
      'execute' => 'commerce_coupon_couponrules_action_is_invalid_coupon',
    ),
  );

  return $actions;
}

/**
 * Rules condition callback: Checks if a particular coupon exists in an order.
 */
function commerce_coupon_couponrules_condition_coupon_exists($order, $code) {
  // If we actually received a valid order...
  if (!empty($order)) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    $coupons = $order_wrapper->commerce_coupon_order_reference->value();
    if ($coupons) {
      foreach ($coupons as $coupon) {
        $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
        if (strtolower($coupon_wrapper->commerce_coupon_code->value()) == strtolower($code)) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}


/**
 * Rules condition callback: Checks if a particular coupon exists in an order.
 */
function commerce_coupon_couponrules_condition_coupons_exists($order, $codes) {
  // If we actually received a valid order...
  if (!empty($order)) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    $coupons = $order_wrapper->commerce_coupon_order_reference->value();
    if ($coupons) {

      // Lower case all codes and filter any invalid codes
      $codes_in_lower_case = array_map('strtolower', $codes);
      $codes_in_lower_case = array_filter($codes_in_lower_case);

      foreach ($coupons as $coupon) {
        $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $coupon);
        if (in_array(strtolower($coupon_wrapper->commerce_coupon_code->value()), $codes_in_lower_case)) {
          return TRUE;
        }
      }
    }
  }

  return FALSE;
}

/**
 * Rules action callback: Validate an individual coupon using its own component.
 * Whitelist rules are evaluated first, so that blacklist rules can override.
 */
function commerce_coupon_couponrules_action_validate($coupon, $order) {
  // If the coupon as it's own rule, invoke it now.
  if ($coupon_rule = rules_config_load(COMMERCE_COUPON_COUPONRULES_WHITELIST_RULE_PREFIX . $coupon->coupon_id)) {
    // If we have a rule, default to invalid until the coupon's rule sets
    // it back to valid.
    $validation_results = &drupal_static('commerce_coupon_action_validation_results');
    $validation_results = FALSE;

    rules_invoke_component(COMMERCE_COUPON_COUPONRULES_WHITELIST_RULE_PREFIX . $coupon->coupon_id, $coupon, $order);
  }

  // If a black list rule is defined, evaluate that after whitelist.
  if ($coupon_rule = rules_config_load(COMMERCE_COUPON_COUPONRULES_BLACKLIST_RULE_PREFIX . $coupon->coupon_id)) {
    rules_invoke_component(COMMERCE_COUPON_COUPONRULES_BLACKLIST_RULE_PREFIX . $coupon->coupon_id, $coupon, $order);
  }
}

/**
 * Rules action callback: Set commerce_coupon_action_validation_results
 * to TRUE, trumps all other coupon validations.
 */
function commerce_coupon_couponrules_action_is_valid_coupon($coupon) {
  $validation_results = &drupal_static('commerce_coupon_action_validation_results');
  $validation_results = TRUE;
}

/**
 * Rules action callback: Set commerce_coupon_action_validation_results
 * to FALSE, trumps all other coupon validations.
 */
function commerce_coupon_couponrules_action_is_invalid_coupon($coupon) {
  $validation_results = &drupal_static('commerce_coupon_action_validation_results');
  $validation_results = FALSE;
}