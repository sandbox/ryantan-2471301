<?php

/**
 * @file
 * Coupon rules default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_coupon_couponrules_default_rules_configuration() {
  $rules = array();

  // Rule to check individual coupon component.
  $rule = '{ "commerce_coupon_couponrules_validate_coupon_rules" : {
      "LABEL" : "Coupon Validation: Check coupon whitelist and blacklist rules",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-10",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Coupon", "Commerce Coupon Rules" ],
      "REQUIRES" : [ "commerce_coupon_couponrules", "commerce_coupon" ],
      "ON" : { "commerce_coupon_validate" : [] },
      "DO" : [
        { "commerce_coupon_couponrules_action_validate" : { "coupon" : [ "coupon" ], "commerce_order" : [ "commerce_order" ] } }
      ]
    }
  }';
  $rules['commerce_coupon_couponrules_validate_coupon_rules'] = rules_import($rule);

  return $rules;
}
